﻿namespace UmfrageAuswertung
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.fr1rb1b = new System.Windows.Forms.RadioButton();
            this.fr1rb2b = new System.Windows.Forms.RadioButton();
            this.fr1rb3b = new System.Windows.Forms.RadioButton();
            this.fr1rb6b = new System.Windows.Forms.RadioButton();
            this.fr1rb5b = new System.Windows.Forms.RadioButton();
            this.fr1rb4b = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.fr2rb1 = new System.Windows.Forms.RadioButton();
            this.fr2rb2 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.fr3cb1 = new System.Windows.Forms.CheckBox();
            this.fr3cb2 = new System.Windows.Forms.CheckBox();
            this.fr3cb9 = new System.Windows.Forms.CheckBox();
            this.fr3cb3 = new System.Windows.Forms.CheckBox();
            this.fr3cb4 = new System.Windows.Forms.CheckBox();
            this.fr3cb11 = new System.Windows.Forms.CheckBox();
            this.fr3cb5 = new System.Windows.Forms.CheckBox();
            this.fr3cb6 = new System.Windows.Forms.CheckBox();
            this.fr3cb10 = new System.Windows.Forms.CheckBox();
            this.fr3cb12 = new System.Windows.Forms.CheckBox();
            this.fr3cb13 = new System.Windows.Forms.CheckBox();
            this.fr3cb14 = new System.Windows.Forms.CheckBox();
            this.fr3cb7 = new System.Windows.Forms.CheckBox();
            this.fr3cb8 = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.fr4rb1 = new System.Windows.Forms.RadioButton();
            this.fr4rb2 = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.fr5rb1 = new System.Windows.Forms.RadioButton();
            this.fr5rb2 = new System.Windows.Forms.RadioButton();
            this.fr5rb4 = new System.Windows.Forms.RadioButton();
            this.fr5rb3 = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnBeenden = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.fr1rb5 = new System.Windows.Forms.RadioButton();
            this.fr1rb4 = new System.Windows.Forms.RadioButton();
            this.fr1rb6 = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.fr1rb3 = new System.Windows.Forms.RadioButton();
            this.fr1rb2 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.fr1rb1 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fr1rb1b
            // 
            this.fr1rb1b.AutoSize = true;
            this.fr1rb1b.Location = new System.Drawing.Point(13, 3);
            this.fr1rb1b.Name = "fr1rb1b";
            this.fr1rb1b.Size = new System.Drawing.Size(31, 17);
            this.fr1rb1b.TabIndex = 0;
            this.fr1rb1b.TabStop = true;
            this.fr1rb1b.Text = "1";
            this.fr1rb1b.UseVisualStyleBackColor = true;
            // 
            // fr1rb2b
            // 
            this.fr1rb2b.AutoSize = true;
            this.fr1rb2b.Location = new System.Drawing.Point(104, 3);
            this.fr1rb2b.Name = "fr1rb2b";
            this.fr1rb2b.Size = new System.Drawing.Size(31, 17);
            this.fr1rb2b.TabIndex = 0;
            this.fr1rb2b.TabStop = true;
            this.fr1rb2b.Text = "2";
            this.fr1rb2b.UseVisualStyleBackColor = true;
            // 
            // fr1rb3b
            // 
            this.fr1rb3b.AutoSize = true;
            this.fr1rb3b.Location = new System.Drawing.Point(204, 3);
            this.fr1rb3b.Name = "fr1rb3b";
            this.fr1rb3b.Size = new System.Drawing.Size(31, 17);
            this.fr1rb3b.TabIndex = 0;
            this.fr1rb3b.TabStop = true;
            this.fr1rb3b.Text = "3";
            this.fr1rb3b.UseVisualStyleBackColor = true;
            // 
            // fr1rb6b
            // 
            this.fr1rb6b.AutoSize = true;
            this.fr1rb6b.Location = new System.Drawing.Point(204, 26);
            this.fr1rb6b.Name = "fr1rb6b";
            this.fr1rb6b.Size = new System.Drawing.Size(31, 17);
            this.fr1rb6b.TabIndex = 0;
            this.fr1rb6b.TabStop = true;
            this.fr1rb6b.Text = "6";
            this.fr1rb6b.UseVisualStyleBackColor = true;
            // 
            // fr1rb5b
            // 
            this.fr1rb5b.AutoSize = true;
            this.fr1rb5b.Location = new System.Drawing.Point(104, 26);
            this.fr1rb5b.Name = "fr1rb5b";
            this.fr1rb5b.Size = new System.Drawing.Size(31, 17);
            this.fr1rb5b.TabIndex = 0;
            this.fr1rb5b.TabStop = true;
            this.fr1rb5b.Text = "5";
            this.fr1rb5b.UseVisualStyleBackColor = true;
            // 
            // fr1rb4b
            // 
            this.fr1rb4b.AutoSize = true;
            this.fr1rb4b.Location = new System.Drawing.Point(13, 26);
            this.fr1rb4b.Name = "fr1rb4b";
            this.fr1rb4b.Size = new System.Drawing.Size(31, 17);
            this.fr1rb4b.TabIndex = 0;
            this.fr1rb4b.TabStop = true;
            this.fr1rb4b.Text = "4";
            this.fr1rb4b.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(299, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Wie bewerten Sie das derzeitige Intranet nach Bedienbarkeit?";
            // 
            // fr2rb1
            // 
            this.fr2rb1.AutoSize = true;
            this.fr2rb1.Location = new System.Drawing.Point(13, 7);
            this.fr2rb1.Name = "fr2rb1";
            this.fr2rb1.Size = new System.Drawing.Size(36, 17);
            this.fr2rb1.TabIndex = 0;
            this.fr2rb1.TabStop = true;
            this.fr2rb1.Text = "Ja";
            this.fr2rb1.UseVisualStyleBackColor = true;
            // 
            // fr2rb2
            // 
            this.fr2rb2.AutoSize = true;
            this.fr2rb2.Location = new System.Drawing.Point(104, 7);
            this.fr2rb2.Name = "fr2rb2";
            this.fr2rb2.Size = new System.Drawing.Size(47, 17);
            this.fr2rb2.TabIndex = 0;
            this.fr2rb2.TabStop = true;
            this.fr2rb2.Text = "Nein";
            this.fr2rb2.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 216);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Gefällt Ihnen das jetztige Design?";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Wenn nein, warum nicht?:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(48, 201);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Frage 2:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(48, 336);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Frage 3:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(48, 356);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(483, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Haben Sie in der Vergangenheit Anwendungen und Informationen aus folgenden Bereic" +
                "hen genutzt?";
            // 
            // fr3cb1
            // 
            this.fr3cb1.AutoSize = true;
            this.fr3cb1.Location = new System.Drawing.Point(114, 387);
            this.fr3cb1.Name = "fr3cb1";
            this.fr3cb1.Size = new System.Drawing.Size(65, 17);
            this.fr3cb1.TabIndex = 3;
            this.fr3cb1.Text = "Projekte";
            this.fr3cb1.UseVisualStyleBackColor = true;
            // 
            // fr3cb2
            // 
            this.fr3cb2.AutoSize = true;
            this.fr3cb2.Location = new System.Drawing.Point(267, 387);
            this.fr3cb2.Name = "fr3cb2";
            this.fr3cb2.Size = new System.Drawing.Size(90, 17);
            this.fr3cb2.TabIndex = 3;
            this.fr3cb2.Text = "Zeiterfassung";
            this.fr3cb2.UseVisualStyleBackColor = true;
            // 
            // fr3cb9
            // 
            this.fr3cb9.AutoSize = true;
            this.fr3cb9.Location = new System.Drawing.Point(114, 479);
            this.fr3cb9.Name = "fr3cb9";
            this.fr3cb9.Size = new System.Drawing.Size(136, 17);
            this.fr3cb9.TabIndex = 3;
            this.fr3cb9.Text = "Formulare u. Richtlinien";
            this.fr3cb9.UseVisualStyleBackColor = true;
            // 
            // fr3cb3
            // 
            this.fr3cb3.AutoSize = true;
            this.fr3cb3.Location = new System.Drawing.Point(114, 410);
            this.fr3cb3.Name = "fr3cb3";
            this.fr3cb3.Size = new System.Drawing.Size(96, 17);
            this.fr3cb3.TabIndex = 3;
            this.fr3cb3.Text = "RL Strategisch";
            this.fr3cb3.UseVisualStyleBackColor = true;
            // 
            // fr3cb4
            // 
            this.fr3cb4.AutoSize = true;
            this.fr3cb4.Location = new System.Drawing.Point(267, 410);
            this.fr3cb4.Name = "fr3cb4";
            this.fr3cb4.Size = new System.Drawing.Size(86, 17);
            this.fr3cb4.TabIndex = 3;
            this.fr3cb4.Text = "Telefonbuch";
            this.fr3cb4.UseVisualStyleBackColor = true;
            // 
            // fr3cb11
            // 
            this.fr3cb11.AutoSize = true;
            this.fr3cb11.Location = new System.Drawing.Point(114, 502);
            this.fr3cb11.Name = "fr3cb11";
            this.fr3cb11.Size = new System.Drawing.Size(76, 17);
            this.fr3cb11.TabIndex = 3;
            this.fr3cb11.Text = "Newsletter";
            this.fr3cb11.UseVisualStyleBackColor = true;
            // 
            // fr3cb5
            // 
            this.fr3cb5.AutoSize = true;
            this.fr3cb5.Location = new System.Drawing.Point(114, 433);
            this.fr3cb5.Name = "fr3cb5";
            this.fr3cb5.Size = new System.Drawing.Size(78, 17);
            this.fr3cb5.TabIndex = 3;
            this.fr3cb5.Text = "Speiseplan";
            this.fr3cb5.UseVisualStyleBackColor = true;
            // 
            // fr3cb6
            // 
            this.fr3cb6.AutoSize = true;
            this.fr3cb6.Location = new System.Drawing.Point(267, 433);
            this.fr3cb6.Name = "fr3cb6";
            this.fr3cb6.Size = new System.Drawing.Size(62, 17);
            this.fr3cb6.TabIndex = 3;
            this.fr3cb6.Text = "RL Mail";
            this.fr3cb6.UseVisualStyleBackColor = true;
            // 
            // fr3cb10
            // 
            this.fr3cb10.AutoSize = true;
            this.fr3cb10.Location = new System.Drawing.Point(267, 479);
            this.fr3cb10.Name = "fr3cb10";
            this.fr3cb10.Size = new System.Drawing.Size(57, 17);
            this.fr3cb10.TabIndex = 3;
            this.fr3cb10.Text = "eShop";
            this.fr3cb10.UseVisualStyleBackColor = true;
            // 
            // fr3cb12
            // 
            this.fr3cb12.AutoSize = true;
            this.fr3cb12.Location = new System.Drawing.Point(267, 502);
            this.fr3cb12.Name = "fr3cb12";
            this.fr3cb12.Size = new System.Drawing.Size(98, 17);
            this.fr3cb12.TabIndex = 3;
            this.fr3cb12.Text = "Berichte/Listen";
            this.fr3cb12.UseVisualStyleBackColor = true;
            // 
            // fr3cb13
            // 
            this.fr3cb13.AutoSize = true;
            this.fr3cb13.Location = new System.Drawing.Point(114, 525);
            this.fr3cb13.Name = "fr3cb13";
            this.fr3cb13.Size = new System.Drawing.Size(67, 17);
            this.fr3cb13.TabIndex = 3;
            this.fr3cb13.Text = "RL Story";
            this.fr3cb13.UseVisualStyleBackColor = true;
            // 
            // fr3cb14
            // 
            this.fr3cb14.AutoSize = true;
            this.fr3cb14.Location = new System.Drawing.Point(267, 525);
            this.fr3cb14.Name = "fr3cb14";
            this.fr3cb14.Size = new System.Drawing.Size(82, 17);
            this.fr3cb14.TabIndex = 3;
            this.fr3cb14.Text = "Wörterbuch";
            this.fr3cb14.UseVisualStyleBackColor = true;
            // 
            // fr3cb7
            // 
            this.fr3cb7.AutoSize = true;
            this.fr3cb7.Location = new System.Drawing.Point(114, 456);
            this.fr3cb7.Name = "fr3cb7";
            this.fr3cb7.Size = new System.Drawing.Size(96, 17);
            this.fr3cb7.TabIndex = 3;
            this.fr3cb7.Text = "Presseberichte";
            this.fr3cb7.UseVisualStyleBackColor = true;
            // 
            // fr3cb8
            // 
            this.fr3cb8.AutoSize = true;
            this.fr3cb8.Location = new System.Drawing.Point(267, 456);
            this.fr3cb8.Name = "fr3cb8";
            this.fr3cb8.Size = new System.Drawing.Size(78, 17);
            this.fr3cb8.TabIndex = 3;
            this.fr3cb8.Text = "RL College";
            this.fr3cb8.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(611, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Frage 4:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(611, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(244, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Würden Sie in Zukunft organisatorische Termine...";
            // 
            // fr4rb1
            // 
            this.fr4rb1.AutoSize = true;
            this.fr4rb1.Location = new System.Drawing.Point(15, 6);
            this.fr4rb1.Name = "fr4rb1";
            this.fr4rb1.Size = new System.Drawing.Size(36, 17);
            this.fr4rb1.TabIndex = 0;
            this.fr4rb1.TabStop = true;
            this.fr4rb1.Text = "Ja";
            this.fr4rb1.UseVisualStyleBackColor = true;
            // 
            // fr4rb2
            // 
            this.fr4rb2.AutoSize = true;
            this.fr4rb2.Location = new System.Drawing.Point(106, 6);
            this.fr4rb2.Name = "fr4rb2";
            this.fr4rb2.Size = new System.Drawing.Size(47, 17);
            this.fr4rb2.TabIndex = 0;
            this.fr4rb2.TabStop = true;
            this.fr4rb2.Text = "Nein";
            this.fr4rb2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(611, 179);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(229, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Wie oft verwenden Sie Funktionen im Intranet?";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(611, 161);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Frage 5:";
            // 
            // fr5rb1
            // 
            this.fr5rb1.AutoSize = true;
            this.fr5rb1.Location = new System.Drawing.Point(16, 6);
            this.fr5rb1.Name = "fr5rb1";
            this.fr5rb1.Size = new System.Drawing.Size(151, 17);
            this.fr5rb1.TabIndex = 0;
            this.fr5rb1.TabStop = true;
            this.fr5rb1.Text = "Weniger als 1x pro Woche";
            this.fr5rb1.UseVisualStyleBackColor = true;
            // 
            // fr5rb2
            // 
            this.fr5rb2.AutoSize = true;
            this.fr5rb2.Location = new System.Drawing.Point(211, 6);
            this.fr5rb2.Name = "fr5rb2";
            this.fr5rb2.Size = new System.Drawing.Size(92, 17);
            this.fr5rb2.TabIndex = 0;
            this.fr5rb2.TabStop = true;
            this.fr5rb2.Text = "1x pro Woche";
            this.fr5rb2.UseVisualStyleBackColor = true;
            // 
            // fr5rb4
            // 
            this.fr5rb4.AutoSize = true;
            this.fr5rb4.Location = new System.Drawing.Point(211, 29);
            this.fr5rb4.Name = "fr5rb4";
            this.fr5rb4.Size = new System.Drawing.Size(60, 17);
            this.fr5rb4.TabIndex = 0;
            this.fr5rb4.TabStop = true;
            this.fr5rb4.Text = "Täglich";
            this.fr5rb4.UseVisualStyleBackColor = true;
            // 
            // fr5rb3
            // 
            this.fr5rb3.AutoSize = true;
            this.fr5rb3.Location = new System.Drawing.Point(16, 29);
            this.fr5rb3.Name = "fr5rb3";
            this.fr5rb3.Size = new System.Drawing.Size(126, 17);
            this.fr5rb3.TabIndex = 0;
            this.fr5rb3.TabStop = true;
            this.fr5rb3.Text = "Mehrmals pro Woche";
            this.fr5rb3.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(611, 288);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(337, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Welche Informationen und Funktionen wünschen Sie sich in Zukunft?";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(611, 270);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Frage 6:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(611, 376);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(229, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Sonstige Bemerkungen bezüglich des Intranets";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(611, 361);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Frage 7:";
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(795, 519);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(89, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "Abschicken";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnBeenden
            // 
            this.btnBeenden.Location = new System.Drawing.Point(900, 519);
            this.btnBeenden.Name = "btnBeenden";
            this.btnBeenden.Size = new System.Drawing.Size(75, 23);
            this.btnBeenden.TabIndex = 4;
            this.btnBeenden.Text = "Beenden";
            this.btnBeenden.UseVisualStyleBackColor = true;
            this.btnBeenden.Click += new System.EventHandler(this.btnBeenden_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.fr1rb2b);
            this.panel2.Controls.Add(this.fr1rb1b);
            this.panel2.Controls.Add(this.fr1rb3b);
            this.panel2.Controls.Add(this.fr1rb6b);
            this.panel2.Controls.Add(this.fr1rb5b);
            this.panel2.Controls.Add(this.fr1rb4b);
            this.panel2.Location = new System.Drawing.Point(51, 133);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(251, 52);
            this.panel2.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.fr2rb2);
            this.panel3.Controls.Add(this.fr2rb1);
            this.panel3.Location = new System.Drawing.Point(51, 232);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(162, 30);
            this.panel3.TabIndex = 7;
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(83, 380);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(299, 170);
            this.panel4.TabIndex = 8;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.fr4rb2);
            this.panel5.Controls.Add(this.fr4rb1);
            this.panel5.Location = new System.Drawing.Point(624, 61);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(166, 29);
            this.panel5.TabIndex = 9;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.fr5rb2);
            this.panel6.Controls.Add(this.fr5rb1);
            this.panel6.Controls.Add(this.fr5rb4);
            this.panel6.Controls.Add(this.fr5rb3);
            this.panel6.Location = new System.Drawing.Point(599, 203);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(321, 52);
            this.panel6.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(51, 288);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Hinzufügen";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(143, 290);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(308, 20);
            this.textBox1.TabIndex = 12;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(591, 115);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Hinzufügen";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(683, 117);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(308, 20);
            this.textBox2.TabIndex = 12;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(591, 319);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "Hinzufügen";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(683, 321);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(308, 20);
            this.textBox3.TabIndex = 12;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(592, 404);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 11;
            this.button4.Text = "Hinzufügen";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(684, 406);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(308, 20);
            this.textBox4.TabIndex = 12;
            // 
            // fr1rb5
            // 
            this.fr1rb5.AutoSize = true;
            this.fr1rb5.Location = new System.Drawing.Point(120, 65);
            this.fr1rb5.Name = "fr1rb5";
            this.fr1rb5.Size = new System.Drawing.Size(31, 17);
            this.fr1rb5.TabIndex = 0;
            this.fr1rb5.TabStop = true;
            this.fr1rb5.Text = "5";
            this.fr1rb5.UseVisualStyleBackColor = true;
            // 
            // fr1rb4
            // 
            this.fr1rb4.AutoSize = true;
            this.fr1rb4.Location = new System.Drawing.Point(29, 65);
            this.fr1rb4.Name = "fr1rb4";
            this.fr1rb4.Size = new System.Drawing.Size(31, 17);
            this.fr1rb4.TabIndex = 0;
            this.fr1rb4.TabStop = true;
            this.fr1rb4.Text = "4";
            this.fr1rb4.UseVisualStyleBackColor = true;
            // 
            // fr1rb6
            // 
            this.fr1rb6.AutoSize = true;
            this.fr1rb6.Location = new System.Drawing.Point(220, 65);
            this.fr1rb6.Name = "fr1rb6";
            this.fr1rb6.Size = new System.Drawing.Size(31, 17);
            this.fr1rb6.TabIndex = 0;
            this.fr1rb6.TabStop = true;
            this.fr1rb6.Text = "6";
            this.fr1rb6.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Frage 1:";
            // 
            // fr1rb3
            // 
            this.fr1rb3.AutoSize = true;
            this.fr1rb3.Location = new System.Drawing.Point(220, 42);
            this.fr1rb3.Name = "fr1rb3";
            this.fr1rb3.Size = new System.Drawing.Size(31, 17);
            this.fr1rb3.TabIndex = 0;
            this.fr1rb3.TabStop = true;
            this.fr1rb3.Text = "3";
            this.fr1rb3.UseVisualStyleBackColor = true;
            // 
            // fr1rb2
            // 
            this.fr1rb2.AutoSize = true;
            this.fr1rb2.Location = new System.Drawing.Point(120, 42);
            this.fr1rb2.Name = "fr1rb2";
            this.fr1rb2.Size = new System.Drawing.Size(31, 17);
            this.fr1rb2.TabIndex = 0;
            this.fr1rb2.TabStop = true;
            this.fr1rb2.Text = "2";
            this.fr1rb2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Wie bewerten Sie das derzeitige Intranet nach Inhalt";
            // 
            // fr1rb1
            // 
            this.fr1rb1.AutoSize = true;
            this.fr1rb1.Location = new System.Drawing.Point(29, 42);
            this.fr1rb1.Name = "fr1rb1";
            this.fr1rb1.Size = new System.Drawing.Size(31, 17);
            this.fr1rb1.TabIndex = 0;
            this.fr1rb1.TabStop = true;
            this.fr1rb1.Text = "1";
            this.fr1rb1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.fr1rb1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.fr1rb2);
            this.groupBox1.Controls.Add(this.fr1rb3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.fr1rb6);
            this.groupBox1.Controls.Add(this.fr1rb4);
            this.groupBox1.Controls.Add(this.fr1rb5);
            this.groupBox1.Location = new System.Drawing.Point(39, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(314, 89);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(684, 479);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(117, 20);
            this.textBox5.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(684, 456);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Fragebogennr.";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 573);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnBeenden);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.fr3cb14);
            this.Controls.Add(this.fr3cb8);
            this.Controls.Add(this.fr3cb6);
            this.Controls.Add(this.fr3cb12);
            this.Controls.Add(this.fr3cb11);
            this.Controls.Add(this.fr3cb4);
            this.Controls.Add(this.fr3cb13);
            this.Controls.Add(this.fr3cb7);
            this.Controls.Add(this.fr3cb10);
            this.Controls.Add(this.fr3cb5);
            this.Controls.Add(this.fr3cb9);
            this.Controls.Add(this.fr3cb3);
            this.Controls.Add(this.fr3cb2);
            this.Controls.Add(this.fr3cb1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel6);
            this.Name = "Form1";
            this.Text = "Fragebogen Auswertung";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton fr1rb1b;
        private System.Windows.Forms.RadioButton fr1rb2b;
        private System.Windows.Forms.RadioButton fr1rb3b;
        private System.Windows.Forms.RadioButton fr1rb6b;
        private System.Windows.Forms.RadioButton fr1rb5b;
        private System.Windows.Forms.RadioButton fr1rb4b;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton fr2rb1;
        private System.Windows.Forms.RadioButton fr2rb2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox fr3cb1;
        private System.Windows.Forms.CheckBox fr3cb2;
        private System.Windows.Forms.CheckBox fr3cb9;
        private System.Windows.Forms.CheckBox fr3cb3;
        private System.Windows.Forms.CheckBox fr3cb4;
        private System.Windows.Forms.CheckBox fr3cb11;
        private System.Windows.Forms.CheckBox fr3cb5;
        private System.Windows.Forms.CheckBox fr3cb6;
        private System.Windows.Forms.CheckBox fr3cb10;
        private System.Windows.Forms.CheckBox fr3cb12;
        private System.Windows.Forms.CheckBox fr3cb13;
        private System.Windows.Forms.CheckBox fr3cb14;
        private System.Windows.Forms.CheckBox fr3cb7;
        private System.Windows.Forms.CheckBox fr3cb8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RadioButton fr4rb1;
        private System.Windows.Forms.RadioButton fr4rb2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton fr5rb1;
        private System.Windows.Forms.RadioButton fr5rb2;
        private System.Windows.Forms.RadioButton fr5rb4;
        private System.Windows.Forms.RadioButton fr5rb3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnBeenden;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.RadioButton fr1rb5;
        private System.Windows.Forms.RadioButton fr1rb4;
        private System.Windows.Forms.RadioButton fr1rb6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton fr1rb3;
        private System.Windows.Forms.RadioButton fr1rb2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton fr1rb1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label11;
    }
}

