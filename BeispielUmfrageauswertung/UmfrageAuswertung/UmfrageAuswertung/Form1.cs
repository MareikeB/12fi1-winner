﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UmfrageAuswertung;
using System.Configuration;
using System.Data.OleDb;

namespace UmfrageAuswertung
{
    public partial class Form1 : Form
    {
        public List<string> txtb1;
        public List<string> _txttrans;
        public List<string> _txttrans1;
        public List<string> _txttrans2;
        public List<string> _txttrans3;

        public int secure = 0;
        protected int c = 0;
        string ID;
        
        string Bearbeiter;
        public int[,] rbAWs = new int[5, 6] { { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 } };
        public int[] cbAWs = new int[14];
        protected string Pfad;

        public int testi = 0;
        
        public AppSettingsReader config = new AppSettingsReader();

        public Form1()
        {
            InitializeComponent();
            Pfad = (string)config.GetValue("Datenbankpfad", typeof(string));
            Bearbeiter = (string)config.GetValue("Bearbeiter", typeof(string));

        }

        private void btnBeenden_Click(object sender, EventArgs e)
        {
            switch (MessageBox.Show("Wirklich Beenden? Einträge werden nicht gespeichert!",
                        "Einträge werden nicht gespeichert!",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question))
            {
                case DialogResult.Yes:
                    Application.Exit();
                    break;

                case DialogResult.No:
                    // "No" processing
                    break;              
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {            
            ID = textBox5.Text;
            rbAWs = Checker();
            cbAWs = Checker2();
            cbAuswerter();
            Auswerter();
            ofAuswerter(ID = textBox5.Text);
            ofAuswerter2(ID = textBox5.Text);
            ofAuswerter3(ID = textBox5.Text);
            ofAuswerter4(ID = textBox5.Text);            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2(this, this.Pfad);
            form2.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3(this, this.Pfad);
            form3.Show();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form4 form4 = new Form4(this, this.Pfad);
            form4.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form5 form5 = new Form5(this, this.Pfad);
            form5.Show();
        }

        public string gesWerte
        {
            get { return textBox1.Text; }
            set { this.textBox1.Text = value.ToString(); }
        }

        public List<string> txttrans 
        {
            set { _txttrans = value; }
        }

        public string gesWerte1
        {
            get { return textBox2.Text; }
            set { this.textBox2.Text = value.ToString(); }
        }

        public List<string> txttrans1 
        {
            set { _txttrans1 = value; }
        }

        public string gesWerte2
        {
            get { return textBox3.Text; }
            set { this.textBox3.Text = value.ToString(); }
        }

        public List<string> txttrans2 
        {
            set { _txttrans2 = value; }
        }

        public string gesWerte3
        {
            get { return textBox4.Text; }
            set { this.textBox4.Text = value.ToString(); }
        }

        public List<string> txttrans3 
        {
            set { _txttrans2 = value; }
        }

        public int[,] Checker()
        {
            int[,] Tester = new int[5, 6];

            if (fr1rb1.Checked)
            {
                Tester[0, 0] = 1;
            }

            if (fr1rb2.Checked)
            {
                Tester[0, 1] = 1;
            }

            if (fr1rb3.Checked)
            {
                Tester[0, 2] = 1;
            }
            if (fr1rb4.Checked)
            {
                Tester[0, 3] = 1;
            }

            if (fr1rb5.Checked)
            {
                Tester[0, 4] = 1;
            }

            if (fr1rb6.Checked)
            {
                Tester[0, 5] = 1;
            }

            if (fr1rb1b.Checked)
            {
                Tester[1, 0] = 1;
            }

            if (fr1rb2b.Checked)
            {
                Tester[1, 1] = 1;
            }

            if (fr1rb3b.Checked)
            {
                Tester[1, 2] = 1;
            }
            if (fr1rb4b.Checked)
            {
                Tester[1, 3] = 1;
            }

            if (fr1rb5b.Checked)
            {
                Tester[1, 4] = 1;
            }

            if (fr1rb6b.Checked)
            {
                Tester[1, 5] = 1;
            }

            if (fr2rb1.Checked)
            {
                Tester[2, 0] = 1;
            }

            if (fr2rb2.Checked)
            {
                Tester[2, 1] = 1;
            }

            if (fr4rb1.Checked)
            {
                Tester[3, 0] = 1;
            }

            if (fr4rb2.Checked)
            {
                Tester[3, 1] = 1;
            }

            if (fr5rb1.Checked)
            {
                Tester[4, 0] = 1;
            }

            if (fr5rb2.Checked)
            {
                Tester[4, 1] = 1;
            }

            if (fr5rb3.Checked)
            {
                Tester[4, 2] = 1;
            }

            if (fr5rb4.Checked)
            {
                Tester[4, 3] = 1;
            }


            return Tester;
        }

        public int[] Checker2()
        {
            int[] Tester = new int[14];

            if (fr3cb1.Checked)
            {
                Tester[0] = 1;
            }

            if (fr3cb2.Checked)
            {
                Tester[1] = 1;
            }

            if (fr3cb3.Checked)
            {
                Tester[2] = 1;
            }

            if (fr3cb4.Checked)
            {
                Tester[3] = 1;
            }

            if (fr3cb5.Checked)
            {
                Tester[4] = 1;
            }

            if (fr3cb6.Checked)
            {
                Tester[5] = 1;
            }

            if (fr3cb7.Checked)
            {
                Tester[6] = 1;
            }

            if (fr3cb8.Checked)
            {
                Tester[7] = 1;
            }

            if (fr3cb9.Checked)
            {
                Tester[8] = 1;
            }

            if (fr3cb10.Checked)
            {
                Tester[9] = 1;
            }

            if (fr3cb11.Checked)
            {
                Tester[10] = 1;
            }

            if (fr3cb12.Checked)
            {
                Tester[11] = 1;
            }

            if (fr3cb13.Checked)
            {
                Tester[12] = 1;
            }

            if (fr3cb14.Checked)
            {
                Tester[13] = 1;
            }
            
            
            return Tester;
        }

        public void Auswerter()
        {
            string ID = textBox5.Text;


            if (ID != "")
            {
                List<string> IDs = new List<string>();
                int c = -1;

                using (OleDbConnection con = new OleDbConnection(
                 @"Provider=Microsoft.Jet.OLEDB.4.0;
             Data Source=" + Pfad))
                {
                    con.Open();

                    string strSQL = "SELECT ID FROM Frage1";


                    OleDbCommand cmd = new OleDbCommand(strSQL, con);
                    OleDbDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        IDs.Add(dr[0].ToString());
                    }

                    c = IDs.IndexOf(ID);


                    if (c == -1)
                    {
                        string insertStatement = "INSERT INTO Frage1 "
                            + "(ID, Antwort1, Antwort2, Antwort3, Antwort4, Antwort5, Antwort6,Bearbeiter) "
                            + "VALUES (?,?,?,?,?,?,?,?)";

                        OleDbCommand insertCommand = new OleDbCommand(insertStatement, con);


                        insertCommand.Parameters.Add("ID", OleDbType.Char).Value = ID;
                        insertCommand.Parameters.Add("Antwort1", OleDbType.Char).Value = rbAWs[0, 0];
                        insertCommand.Parameters.Add("Antwort2", OleDbType.Char).Value = rbAWs[0, 1];
                        insertCommand.Parameters.Add("Antwort3", OleDbType.Char).Value = rbAWs[0, 2];
                        insertCommand.Parameters.Add("Antwort4", OleDbType.Char).Value = rbAWs[0, 3];
                        insertCommand.Parameters.Add("Antwort5", OleDbType.Char).Value = rbAWs[0, 4];
                        insertCommand.Parameters.Add("Antwort6", OleDbType.Char).Value = rbAWs[0, 5];
                        insertCommand.Parameters.Add("Bearbeiter", OleDbType.Char).Value = Bearbeiter;

                        try
                        {
                            int count = insertCommand.ExecuteNonQuery();
                        }
                        catch (OleDbException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        string insertStatement2 = "INSERT INTO Frage2 "
                           + "(ID, Antwort1, Antwort2, Antwort3, Antwort4, Antwort5, Antwort6,Bearbeiter) "
                           + "VALUES (?,?,?,?,?,?,?,?)";

                        OleDbCommand insertCommand2 = new OleDbCommand(insertStatement2, con);


                        insertCommand2.Parameters.Add("ID", OleDbType.Char).Value = ID;
                        insertCommand2.Parameters.Add("Antwort1", OleDbType.Char).Value = rbAWs[1, 0];
                        insertCommand2.Parameters.Add("Antwort2", OleDbType.Char).Value = rbAWs[1, 1];
                        insertCommand2.Parameters.Add("Antwort3", OleDbType.Char).Value = rbAWs[1, 2];
                        insertCommand2.Parameters.Add("Antwort4", OleDbType.Char).Value = rbAWs[1, 3];
                        insertCommand2.Parameters.Add("Antwort5", OleDbType.Char).Value = rbAWs[1, 4];
                        insertCommand2.Parameters.Add("Antwort6", OleDbType.Char).Value = rbAWs[1, 5];
                        insertCommand2.Parameters.Add("Bearbeiter", OleDbType.Char).Value = Bearbeiter;

                        try
                        {
                            int count = insertCommand2.ExecuteNonQuery();
                        }
                        catch (OleDbException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        string insertStatement3 = "INSERT INTO Frage3 "
                           + "(ID, Antwort1, Antwort2,Bearbeiter) "
                           + "VALUES (?,?,?,?)";

                        OleDbCommand insertCommand3 = new OleDbCommand(insertStatement3, con);


                        insertCommand3.Parameters.Add("ID", OleDbType.Char).Value = ID;
                        insertCommand3.Parameters.Add("Antwort1", OleDbType.Char).Value = rbAWs[2, 0];
                        insertCommand3.Parameters.Add("Antwort2", OleDbType.Char).Value = rbAWs[2, 1];
                        insertCommand3.Parameters.Add("Bearbeiter", OleDbType.Char).Value = Bearbeiter;

                        try
                        {
                            int count = insertCommand3.ExecuteNonQuery();
                        }
                        catch (OleDbException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        string insertStatement4 = "INSERT INTO Frage4 "
                           + "(ID, Antwort1, Antwort2,Bearbeiter) "
                           + "VALUES (?,?,?,?)";

                        OleDbCommand insertCommand4 = new OleDbCommand(insertStatement4, con);


                        insertCommand4.Parameters.Add("ID", OleDbType.Char).Value = ID;
                        insertCommand4.Parameters.Add("Antwort1", OleDbType.Char).Value = rbAWs[3, 0];
                        insertCommand4.Parameters.Add("Antwort2", OleDbType.Char).Value = rbAWs[3, 1];
                        insertCommand4.Parameters.Add("Bearbeiter", OleDbType.Char).Value = Bearbeiter;

                        try
                        {
                            int count = insertCommand4.ExecuteNonQuery();
                        }
                        catch (OleDbException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }


                        string insertStatement5 = "INSERT INTO Frage5 "
                           + "(ID, Antwort1, Antwort2, Antwort3, Antwort4,Bearbeiter) "
                           + "VALUES (?,?,?,?,?,?)";

                        OleDbCommand insertCommand5 = new OleDbCommand(insertStatement5, con);


                        insertCommand5.Parameters.Add("ID", OleDbType.Char).Value = ID;
                        insertCommand5.Parameters.Add("Antwort1", OleDbType.Char).Value = rbAWs[4, 0];
                        insertCommand5.Parameters.Add("Antwort2", OleDbType.Char).Value = rbAWs[4, 1];
                        insertCommand5.Parameters.Add("Antwort3", OleDbType.Char).Value = rbAWs[4, 2];
                        insertCommand5.Parameters.Add("Antwort4", OleDbType.Char).Value = rbAWs[4, 3];
                        insertCommand5.Parameters.Add("Bearbeiter", OleDbType.Char).Value = Bearbeiter;

                        try
                        {
                            int count = insertCommand5.ExecuteNonQuery();
                        }
                        catch (OleDbException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }


                    }
                    else
                    {
                        MessageBox.Show("ID bereits erfasst. Werte wurden nicht übernommen.");

                        dr.Close();
                    }

                }
            }
            else
            {
                MessageBox.Show("Es muss eine ID übergeben werden");
            }
        }

        public void cbAuswerter()
        {
            List<string> IDs = new List<string>();
            int c = -1;

            using (OleDbConnection con = new OleDbConnection(
             @"Provider=Microsoft.Jet.OLEDB.4.0;
             Data Source=" + Pfad))
            {
                con.Open();

                string strSQL = "SELECT ID FROM cbFrage";


                OleDbCommand cmd = new OleDbCommand(strSQL, con);
                OleDbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    IDs.Add(dr[0].ToString());
                }

                c = IDs.IndexOf(ID);


                if (c == -1)
                {
                    string insertStatement = "INSERT INTO cbFrage "
                        + "(ID, Projekte, Zeiterfassung, Strategisch, Telefonbuch, Speiseplan, Mail, Presseberichte, College, Formulare, eShop, Newsletter, Berichte, Story, Woerterbuch, Bearbeiter) "
                        + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                    OleDbCommand insertCommand = new OleDbCommand(insertStatement, con);


                    insertCommand.Parameters.Add("ID", OleDbType.Char).Value = ID;
                    insertCommand.Parameters.Add("Projekte", OleDbType.Char).Value = cbAWs[0];
                    insertCommand.Parameters.Add("Zeiterfassung", OleDbType.Char).Value = cbAWs[1];
                    insertCommand.Parameters.Add("Strategisch", OleDbType.Char).Value = cbAWs[2];
                    insertCommand.Parameters.Add("Telefonbuch", OleDbType.Char).Value = cbAWs[3];
                    insertCommand.Parameters.Add("Speiseplan", OleDbType.Char).Value = cbAWs[4];
                    insertCommand.Parameters.Add("Mail", OleDbType.Char).Value = cbAWs[5];
                    insertCommand.Parameters.Add("Presseberichte", OleDbType.Char).Value = cbAWs[6];
                    insertCommand.Parameters.Add("College", OleDbType.Char).Value = cbAWs[7];
                    insertCommand.Parameters.Add("Formulare", OleDbType.Char).Value = cbAWs[8];
                    insertCommand.Parameters.Add("eShop", OleDbType.Char).Value = cbAWs[9];
                    insertCommand.Parameters.Add("Newsletter", OleDbType.Char).Value = cbAWs[10];
                    insertCommand.Parameters.Add("Berichte", OleDbType.Char).Value = cbAWs[11];
                    insertCommand.Parameters.Add("Story", OleDbType.Char).Value = cbAWs[12];
                    insertCommand.Parameters.Add("Woerterbuch", OleDbType.Char).Value = cbAWs[13];                    
                    insertCommand.Parameters.Add("Bearbeiter", OleDbType.Char).Value = Bearbeiter;

                    try
                    {
                        int count = insertCommand.ExecuteNonQuery();
                    }
                    catch (OleDbException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        public void ofAuswerter(string ID)
        {
            
            List<string> Inhalte = new List<string>();
            int c = 0;
            while (c <= 5)
            {
                Inhalte.Add(" ");
                c++;
            }
            
            string[] split = textBox1.Text.Split(new Char[] { ';' });

            int i = 0;

            foreach (string s in split)
            {
                if (s.Trim() != "")
                    Inhalte[i] = s;
                i++;
            }

            using (OleDbConnection con = new OleDbConnection(
           @"Provider=Microsoft.Jet.OLEDB.4.0;
             Data Source=" + Pfad))
            {
                con.Open();
                string insertStatement = "INSERT INTO offeneFrage1"
                      + "(ID, AW1, AW2, AW3, AW4, AW5, AW6, Bearbeiter) "
                      + "VALUES (?,?,?,?,?,?,?,?)";

                OleDbCommand insertCommand = new OleDbCommand(insertStatement, con);


                insertCommand.Parameters.Add("ID", OleDbType.Char).Value = ID;
                insertCommand.Parameters.Add("AW1", OleDbType.Char).Value = Inhalte[0];
                insertCommand.Parameters.Add("AW2", OleDbType.Char).Value = Inhalte[1];
                insertCommand.Parameters.Add("AW3", OleDbType.Char).Value = Inhalte[2];
                insertCommand.Parameters.Add("AW4", OleDbType.Char).Value = Inhalte[3];
                insertCommand.Parameters.Add("AW5", OleDbType.Char).Value = Inhalte[4];
                insertCommand.Parameters.Add("AW6", OleDbType.Char).Value = Inhalte[5];              
                insertCommand.Parameters.Add("Bearbeiter", OleDbType.Char).Value = Bearbeiter;

                try
                {
                    int count = insertCommand.ExecuteNonQuery();
                }
                catch (OleDbException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
                    
            
        }

        public void ofAuswerter2(string ID)
        {

            List<string> Inhalte = new List<string>();
            int c = 0;
            while (c <= 5)
            {
                Inhalte.Add(" ");
                c++;
            }

            string[] split = textBox2.Text.Split(new Char[] { ';' });

            int i = 0;

            foreach (string s in split)
            {
                if (s.Trim() != "")
                    Inhalte[i] = s;
                i++;
            }

            using (OleDbConnection con = new OleDbConnection(
           @"Provider=Microsoft.Jet.OLEDB.4.0;
             Data Source=" + Pfad))
            {
                con.Open();
                string insertStatement = "INSERT INTO offeneFrage2"
                      + "(ID, AW1, AW2, AW3, AW4, AW5, AW6, Bearbeiter) "
                      + "VALUES (?,?,?,?,?,?,?,?)";

                OleDbCommand insertCommand = new OleDbCommand(insertStatement, con);


                insertCommand.Parameters.Add("ID", OleDbType.Char).Value = ID;
                insertCommand.Parameters.Add("AW1", OleDbType.Char).Value = Inhalte[0];
                insertCommand.Parameters.Add("AW2", OleDbType.Char).Value = Inhalte[1];
                insertCommand.Parameters.Add("AW3", OleDbType.Char).Value = Inhalte[2];
                insertCommand.Parameters.Add("AW4", OleDbType.Char).Value = Inhalte[3];
                insertCommand.Parameters.Add("AW5", OleDbType.Char).Value = Inhalte[4];
                insertCommand.Parameters.Add("AW6", OleDbType.Char).Value = Inhalte[5];
                insertCommand.Parameters.Add("Bearbeiter", OleDbType.Char).Value = Bearbeiter;

                try
                {
                    int count = insertCommand.ExecuteNonQuery();
                }
                catch (OleDbException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public void ofAuswerter3(string ID)
        {
            
            List<string> Inhalte = new List<string>();
            int c = 0;
            while (c <= 5)
            {
                Inhalte.Add(" ");
                c++;
            }
            
            string[] split = textBox3.Text.Split(new Char[] { ';' });

            int i = 0;

            foreach (string s in split)
            {
                if (s.Trim() != "")
                    Inhalte[i] = s;
                i++;
            }

            using (OleDbConnection con = new OleDbConnection(
           @"Provider=Microsoft.Jet.OLEDB.4.0;
             Data Source=" + Pfad))
            {
                con.Open();
                string insertStatement = "INSERT INTO offeneFrage3"
                      + "(ID, AW1, AW2, AW3, AW4, AW5, AW6, Bearbeiter) "
                      + "VALUES (?,?,?,?,?,?,?,?)";

                OleDbCommand insertCommand = new OleDbCommand(insertStatement, con);


                insertCommand.Parameters.Add("ID", OleDbType.Char).Value = ID;
                insertCommand.Parameters.Add("AW1", OleDbType.Char).Value = Inhalte[0];
                insertCommand.Parameters.Add("AW2", OleDbType.Char).Value = Inhalte[1];
                insertCommand.Parameters.Add("AW3", OleDbType.Char).Value = Inhalte[2];
                insertCommand.Parameters.Add("AW4", OleDbType.Char).Value = Inhalte[3];
                insertCommand.Parameters.Add("AW5", OleDbType.Char).Value = Inhalte[4];
                insertCommand.Parameters.Add("AW6", OleDbType.Char).Value = Inhalte[5];              
                insertCommand.Parameters.Add("Bearbeiter", OleDbType.Char).Value = Bearbeiter;

                try
                {
                    int count = insertCommand.ExecuteNonQuery();
                }
                catch (OleDbException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
                    
            
        }
       
        public void ofAuswerter4(string ID)
        {

            List<string> Inhalte = new List<string>();
            int c = 0;
            while (c <= 5)
            {
                Inhalte.Add(" ");
                c++;
            }

            string[] split = textBox4.Text.Split(new Char[] { ';' });

            int i = 0;

            foreach (string s in split)
            {
                if (s.Trim() != "")
                    Inhalte[i] = s;
                i++;
            }

            using (OleDbConnection con = new OleDbConnection(
           @"Provider=Microsoft.Jet.OLEDB.4.0;
             Data Source=" + Pfad))
            {
                con.Open();
                string insertStatement = "INSERT INTO offeneFrage4"
                      + "(ID, AW1, AW2, AW3, AW4, AW5, AW6, Bearbeiter) "
                      + "VALUES (?,?,?,?,?,?,?,?)";

                OleDbCommand insertCommand = new OleDbCommand(insertStatement, con);


                insertCommand.Parameters.Add("ID", OleDbType.Char).Value = ID;
                insertCommand.Parameters.Add("AW1", OleDbType.Char).Value = Inhalte[0];
                insertCommand.Parameters.Add("AW2", OleDbType.Char).Value = Inhalte[1];
                insertCommand.Parameters.Add("AW3", OleDbType.Char).Value = Inhalte[2];
                insertCommand.Parameters.Add("AW4", OleDbType.Char).Value = Inhalte[3];
                insertCommand.Parameters.Add("AW5", OleDbType.Char).Value = Inhalte[4];
                insertCommand.Parameters.Add("AW6", OleDbType.Char).Value = Inhalte[5];
                insertCommand.Parameters.Add("Bearbeiter", OleDbType.Char).Value = Bearbeiter;

                try
                {
                    int count = insertCommand.ExecuteNonQuery();
                }
                catch (OleDbException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {


                    switch (MessageBox.Show("Alle Werte erfolgreich übertragen. Möchtenst du ein weiteres Formular ausfüllen?",
                            "Formular ausgefüllt.",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question))
                    {
                        case DialogResult.Yes:
                            Application.Restart();
                            break;

                        case DialogResult.No:
                            Application.Exit();
                            break;
                    }

                   
                }

            }


        }     
    }
}

 
              




