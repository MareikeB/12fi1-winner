﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace UmfrageAuswertung
{
    public partial class Form3 : Form
    {
        protected List<string> Werte = new List<string>();
        int c = 0;
        public List<string> ausgWerte = new List<string>();

        StringBuilder Texte = new StringBuilder();
        string gesWerte;
        public string Pfad;

        private Form1 form1 = null;


        public Form3(Form1 f, string DBPfad)
        {
            InitializeComponent();
            this.form1 = f;
            this.Pfad = DBPfad;
            Datenbankabgleich();
        }
            
            
       
        private void btnAbort_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Datenbankabgleich();
        }



    


        private void btnOK_Click(object sender, EventArgs e)
        {
            Texte = WhatIsChecked_Click();
            gesWerte = Texte.ToString();
            this.form1.gesWerte1 = gesWerte;
            this.form1.txttrans1 = ausgWerte;
        
            this.Hide();
            
        }

        private void Datenbankabgleich()
        {
            Werte = Datenbankaufruf();


            while (c < Werte.Count())
            {
                this.checkedListBox1.Items.Add(Werte[c]);
                c++;
            }
        }

        public List<string> Datenbankaufruf()
        {
            List<string> Werte = new List<string>();
            using (OleDbConnection con = new OleDbConnection(
                @"Provider=Microsoft.Jet.OLEDB.4.0;
                Data Source=" + Pfad))
            {

                con.Open();

                string strSQL = "SELECT NAME FROM Tabelle2";

                OleDbCommand cmd = new OleDbCommand(strSQL, con);
                OleDbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Werte.Add(dr[0].ToString());
                }
                con.Close();
                dr.Close();
            }
            return Werte;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string strName = textBox1.Text;

            if (strName != "")
            {
                List<string> Namen = new List<string>();
                int c;

                using (OleDbConnection con = new OleDbConnection(
                 @"Provider=Microsoft.Jet.OLEDB.4.0;
             Data Source=" + Pfad))
                {
                    con.Open();

                    string strSQL = "SELECT NAME FROM Tabelle2";

                    OleDbCommand cmd = new OleDbCommand(strSQL, con);
                    OleDbDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        Namen.Add(dr[0].ToString());
                    }

                    c = Namen.IndexOf(strName);


                    if (c == -1)
                    {
                        string insertStatement = "INSERT INTO Tabelle2 "
                            + "(Name) "
                            + "VALUES (?)";

                        OleDbCommand insertCommand = new OleDbCommand(insertStatement, con);


                        insertCommand.Parameters.Add("Name", OleDbType.Char).Value = strName;

                        try
                        {
                            int count = insertCommand.ExecuteNonQuery();
                        }
                        catch (OleDbException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        finally
                        {
                            dr.Close();

                            Datenbankabgleich();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Wert bereits enthalten, Daten wurden nicht übernommen");
                        dr.Close();
                    }

                }
            }
        }

        private StringBuilder WhatIsChecked_Click()
        {
            // Display in a message box all the items that are checked.
            // Next show the object title and check state for each item selected.
            foreach (object itemChecked in checkedListBox1.CheckedItems)
            {     
                ausgWerte.Add(itemChecked.ToString());
            }

            c = 0;

            while (c < ausgWerte.Count())
            {
                Texte.Append(ausgWerte[c]);
                Texte.Append("; ");
                c++;
            }

            return Texte;

               
            
        }                   
    }
}


